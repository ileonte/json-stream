/*
 * json-stream - a C++17 single-header streaming JSON parser
 * version 0.1.1
 * https://gitlab.com/ileonte/json-stream
 *
 * Licensed under the MIT License <http://opensource.org/licenses/MIT>.
 * Copyright (c) 2018 Ionuț Leonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Poor-man's todo list:
 *
 *  @LIMIT_RESOURCE_USAGE - instances where some kind of limit(s) should be
 *                          enforced to make sure resource usage doesn't
 *                          spiral out of control
 */

#pragma once

#include <type_traits>
#include <cstdint>
#include <cassert>
#include <cerrno>
#include <limits>
#include <vector>
#include <string>
#include <optional>
#include <algorithm>

namespace jsstream {
    using string = std::string;
    using maybe_string = std::optional<string>;

    namespace detail {
        inline constexpr const char true_chars[]  = {'t', 'r', 'u', 'e'};
        inline constexpr const char false_chars[] = {'f', 'a', 'l', 's', 'e'};
        inline constexpr const char null_chars[]  = {'n', 'u', 'l', 'l'};

        template <typename T, size_t N>
        constexpr size_t array_size(const T (&)[N]) { return N; }

        inline bool decode_unicode_escape_char(char ch, uint16_t& where) {
            switch (ch) {
                case '0': [[fallthrough]];
                case '1': [[fallthrough]];
                case '2': [[fallthrough]];
                case '3': [[fallthrough]];
                case '4': [[fallthrough]];
                case '5': [[fallthrough]];
                case '6': [[fallthrough]];
                case '7': [[fallthrough]];
                case '8': [[fallthrough]];
                case '9': {
                    where = (where << 4) + (ch - '0');
                    return true;
                }
                case 'a': [[fallthrough]];
                case 'b': [[fallthrough]];
                case 'c': [[fallthrough]];
                case 'd': [[fallthrough]];
                case 'e': [[fallthrough]];
                case 'f': {
                    where = (where << 4) + (ch - 'a' + 10);
                    return true;
                }
                case 'A': [[fallthrough]];
                case 'B': [[fallthrough]];
                case 'C': [[fallthrough]];
                case 'D': [[fallthrough]];
                case 'E': [[fallthrough]];
                case 'F': {
                    where = (where << 4) + (ch - 'A' + 10);
                    return true;
                }
                default: return false;
            }
        }

        inline bool char32_to_utf8(int32_t codepoint, char (&buffer)[4], size_t& size) {
            if (codepoint < 0) return false;

            if (codepoint < 0x80) {
                buffer[0] = (char)codepoint;
                size = 1;
            } else if (codepoint < 0x800) {
                buffer[0] = 0xc0 + ((codepoint & 0x7c0) >> 6);
                buffer[1] = 0x80 + ((codepoint & 0x03f));
                size = 2;
            } else if (codepoint < 0x10000) {
                buffer[0] = 0xe0 + ((codepoint & 0xf000) >> 12);
                buffer[1] = 0x80 + ((codepoint & 0x0fc0) >> 6);
                buffer[2] = 0x80 + ((codepoint & 0x003f));
                size = 3;
            } else if (codepoint <= 0x10ffff) {
                buffer[0] = 0xf0 + ((codepoint & 0x1c0000) >> 18);
                buffer[1] = 0x80 + ((codepoint & 0x03f000) >> 12);
                buffer[2] = 0x80 + ((codepoint & 0x000fc0) >> 6);
                buffer[3] = 0x80 + ((codepoint & 0x00003f));
                size = 4;
            } else {
                return false;
            }

            return true;
        }

        inline constexpr bool is_ws(char ch) {
            switch (ch) {
                case '\t': [[fallthrough]];
                case '\r': [[fallthrough]];
                case '\n': [[fallthrough]];
                case  ' ': return true;
                default: return false;
            }
        }

        inline constexpr bool is_nl(char ch) {
            return ch == '\n';
        }
    }

    enum class error_type {
        ok,
        aborted_by_handler,
        invalid_escape,
        invalid_unicode_escape,
        invalid_number,
        invalid_bool,
        invalid_null,
        missing_separator,
        early_separator,
        duplicate_separator,
        key_expected,
        value_expected,
        unexpected_character,
    };
    struct parser_error {
        error_type error{error_type::ok};
        int line{1};
        int column{1};
        size_t  stream_offset{0};

        constexpr const char* to_string() const {
            switch (error) {
                case error_type::ok: return "Everything OK";
                case error_type::aborted_by_handler: return "Parsing aborted by event handler";
                case error_type::invalid_escape: return "Invalid escape sequence";
                case error_type::invalid_unicode_escape: return "Invalid UNICODE escape sequence";
                case error_type::invalid_number: return "Invalid numeric value";
                case error_type::invalid_bool: return "Invalid boolean value";
                case error_type::invalid_null: return "Invalid null value";
                case error_type::missing_separator: return "Missing value separator (',')";
                case error_type::early_separator: return "Expected initial value but separator encountered";
                case error_type::duplicate_separator: return "Expected a value but separator encountered";
                case error_type::key_expected: return "Object end encountered but key was expected";
                case error_type::value_expected: return "Array end encountered but value was expected";
                case error_type::unexpected_character: return "Unexpected character encountered";
                default: return "Unknown error";
            }
        }
    };

    enum class state {
        toplevel,

        value_string,
        value_number,
        value_bool,
        value_null,

        array,

        object,
        object_key,
        object_kv_separator,
        object_key_value,
    };

    class parser_base {
    public:
        inline enum state state() const {
            assert(state_stack_.size() >= 1);
            return state_stack_.back();
        }
        inline int depth() const { return depth_; }
        inline int line() const { return line_; }
        inline int column() const { return column_; }
        inline size_t stream_offset() const { return stream_offset_; }

        bool feed(const char* data, size_t data_size) {
            assert(state_stack_.size() >= 1);
            assert(state_stack_.at(0) == state::toplevel);

            progress_status status{};
            const char* start = (const char*)data;
            const char* end = start + data_size;

#define JSS_HANDLE(func) status = func(this, start, end); break
#define JSS_SKIP_WS \
    case '\t': [[fallthrough]]; \
    case '\r': [[fallthrough]]; \
    case '\n': [[fallthrough]]; \
    case ' ': JSS_HANDLE(skip_ws)

            while (start < end) {
                switch (state()) {
                    case state::toplevel: {
                        switch (*start) {
                            default: JSS_HANDLE(on_invalid_char);

                            JSS_SKIP_WS;

                            case '{': JSS_HANDLE(on_object_start);
                            case '[': JSS_HANDLE(on_array_start);
                        }
                        break;
                    }

                    case state::value_string: JSS_HANDLE(on_string_value_data);
                    case state::value_number: JSS_HANDLE(on_number_value_data);
                    case state::value_bool: JSS_HANDLE(on_bool_value_data);
                    case state::value_null: JSS_HANDLE(on_null_value_data);

                    case state::array: {
                        switch (*start) {
                            default: JSS_HANDLE(on_invalid_char);

                            JSS_SKIP_WS;

                            case ',': JSS_HANDLE(on_array_expects_more_values);
                            case ']': JSS_HANDLE(on_array_end);

                            case '{': JSS_HANDLE(on_object_start);
                            case '[': JSS_HANDLE(on_array_start);

                            case '"': JSS_HANDLE(on_string_value_start);

                            case '-': [[fallthrough]];
                            case '0': [[fallthrough]];
                            case '1': [[fallthrough]];
                            case '2': [[fallthrough]];
                            case '3': [[fallthrough]];
                            case '4': [[fallthrough]];
                            case '5': [[fallthrough]];
                            case '6': [[fallthrough]];
                            case '7': [[fallthrough]];
                            case '8': [[fallthrough]];
                            case '9': JSS_HANDLE(on_number_value_start);

                            case 't': [[fallthrough]];
                            case 'f': JSS_HANDLE(on_bool_value_start);

                            case 'n': JSS_HANDLE(on_null_value_start);
                        }
                        break;
                    }

                    case state::object: {
                        switch (*start) {
                            default: JSS_HANDLE(on_invalid_char);

                            JSS_SKIP_WS;

                            case '"': JSS_HANDLE(on_object_key_start);
                            case ',': JSS_HANDLE(on_object_expects_more_values);
                            case '}': JSS_HANDLE(on_object_end);
                        }
                        break;
                    }
                    case state::object_key: JSS_HANDLE(on_object_key_data);
                    case state::object_kv_separator: {
                        switch (*start) {
                            default: JSS_HANDLE(on_invalid_char);

                            JSS_SKIP_WS;

                            case ':': JSS_HANDLE(on_object_kv_separator);
                        }
                        break;
                    }
                    case state::object_key_value: {
                        switch (*start) {
                            default: JSS_HANDLE(on_invalid_char);

                            JSS_SKIP_WS;

                            case '{': JSS_HANDLE(on_object_start);
                            case '[': JSS_HANDLE(on_array_start);

                            case '"': JSS_HANDLE(on_string_value_start);

                            case '-': [[fallthrough]];
                            case '0': [[fallthrough]];
                            case '1': [[fallthrough]];
                            case '2': [[fallthrough]];
                            case '3': [[fallthrough]];
                            case '4': [[fallthrough]];
                            case '5': [[fallthrough]];
                            case '6': [[fallthrough]];
                            case '7': [[fallthrough]];
                            case '8': [[fallthrough]];
                            case '9': JSS_HANDLE(on_number_value_start);

                            case 't': [[fallthrough]];
                            case 'f': JSS_HANDLE(on_bool_value_start);

                            case 'n': JSS_HANDLE(on_null_value_start);
                        }
                        break;
                    }

                    default: {
                        std::abort();
                        break;
                    }
                }

                auto [ok, it] = status;
                assert(it >= start && it <= end);

                for (auto u = start; u != it && u < end; u++) {
                    update_position(*u);
                }

                if (!ok) return false;

                start = it;
            }

#undef JSS_HANDLE
#undef JSS_SKIP_WS

            return true;
        }

    protected:
        virtual bool call_handler_on_object_start(int, const string&) = 0;
        virtual bool call_handler_on_object_end() = 0;
        virtual bool call_handler_on_array_start(int, const string&) = 0;
        virtual bool call_handler_on_array_end() = 0;
        virtual bool call_handler_on_object_value_string(const string&, const string&) = 0;
        virtual bool call_handler_on_object_value_int(const string&, int64_t) = 0;
        virtual bool call_handler_on_object_value_double(const string&, double) = 0;
        virtual bool call_handler_on_object_value_bool(const string&, bool) = 0;
        virtual bool call_handler_on_object_value_null(const string&) = 0;
        virtual bool call_handler_on_array_value_string(const string&) = 0;
        virtual bool call_handler_on_array_value_int(int64_t) = 0;
        virtual bool call_handler_on_array_value_double(double) = 0;
        virtual bool call_handler_on_array_value_bool(bool) = 0;
        virtual bool call_handler_on_array_value_null() = 0;
        virtual bool call_handler_on_parse_error(const parser_error&) = 0;
        virtual void call_handler_reset() = 0;

        struct progress_status {
            bool ok;
            const char* it;
        };

        inline void state_push(enum state s) {
            state_stack_.push_back(s);
        }

        inline void state_inplace_transition(enum state s) {
            assert(state_stack_.size() >= 2);
            state_stack_.back() = s;
        }

        inline void state_pop() {
            assert(state_stack_.size() >= 2);
            state_stack_.pop_back();
        }

        inline void update_position(char ch) {
            if (detail::is_nl(ch)) {
                column_ = 1;
                line_ += 1;
            } else {
                column_ += 1;
            }
            stream_offset_ += 1;
        }

        static inline progress_status skip_ws(parser_base*, const char* start, const char* end) {
            while ((start < end) && detail::is_ws(*start)) start++;
            return {true, start};
        }

        static inline progress_status null_char_handler(parser_base*, const char*, const char*) {
            return {true, nullptr};
        }
        static inline progress_status deny_char_handler(parser_base*, const char*, const char*) {
            return {false, nullptr};
        }

        static inline progress_status on_invalid_char(parser_base* p,
                                                      const char* start,
                                                      const char* end)
        {
            return on_error(p, error_type::unexpected_character, start, end);
        }

        static inline progress_status on_error(parser_base* p, error_type error,
                                               const char* start, const char*)
        {
            p->call_handler_on_parse_error({error, p->line_, p->column_, p->stream_offset_});
            return {false, start};
        }

        struct object_state {
            int expected_values{0};
            int completed_values{0};
            string last_key;
        };
        std::vector<object_state> object_states_;

        struct array_state {
            int expected_values{0};
            int completed_values{0};
        };
        std::vector<array_state> array_states_;

        /* value buffer */
        enum class value_type {
            none,
            string,
            number,
            boolean,
            null,
        };
        enum class escape_type {
            none,
            simple,

            /* "usp" == "Unicode Surrogate Pair" */
            usp1_0,
            usp1_1,
            usp1_2,
            usp1_3,
            usp2_slash,
            usp2_u,
            usp2_0,
            usp2_1,
            usp2_2,
            usp2_3,
        };
        value_type value_type_{value_type::none};
        escape_type in_escape_{escape_type::none};
        uint16_t surrogate_pair_1_{0};
        uint16_t surrogate_pair_2_{0};
        string value_buffer_{};
        bool looks_like_double_{false};

        void reset_value_buffers() {
            value_type_ = value_type::none;
            in_escape_ = escape_type::none;
            value_buffer_.clear();
            looks_like_double_ = false;
        }

        /* @TODO @LIMIT_RESOURCE_USAGE - make sure we don't buffer infinatelly */
        progress_status buffer_string_value(const char* start, const char* end, maybe_string& ret) {
            assert(value_type_ == value_type::string);
            const char* it = start;

            while (it < end) {
                while (in_escape_ == escape_type::none) {
                    switch (*it++) {
                        case '\\': {
                            value_buffer_.append(start, it - start - 1);
                            in_escape_ = escape_type::simple;
                            if (it == end) return {true, it};
                            break;
                        }
                        case '"': {
                            value_buffer_.append(start, it - start - 1);
                            ret = std::move(value_buffer_);
                            return {true, it};
                        }
                        default: {
                            assert(it <= end);
                            if (it == end) {
                                value_buffer_.append(start, it - start);
                                return {true, it};
                            }
                            break;
                        }
                    }
                }

                switch (in_escape_) {
                    case escape_type::simple: {
                        in_escape_ = escape_type::none;

                        switch (*it++) {
                            case '"': [[fallthrough]];
                            case '\\': [[fallthrough]];
                            case '/': {
                                value_buffer_.append(1, *(it - 1));
                                break;
                            }
                            case 'b': {
                                value_buffer_.append(1, '\b');
                                break;
                            }
                            case 'n': {
                                value_buffer_.append(1, '\n');
                                break;
                            }
                            case 'r': {
                                value_buffer_.append(1, '\r');
                                break;
                            }
                            case 't': {
                                value_buffer_.append(1, '\t');
                                break;
                            }
                            case 'u': {
                                in_escape_ = escape_type::usp1_0;
                                surrogate_pair_1_ = 0;
                                break;
                            }
                            default: {
                                if (it == end) return {true, it};

                                /* error: invalid escape sequence */
                                return on_error(this, error_type::invalid_escape, it, end);
                            }
                        }

                        start = it;
                        break;
                    }

                    case escape_type::usp1_0: [[fallthrough]];
                    case escape_type::usp1_1: [[fallthrough]];
                    case escape_type::usp1_2: [[fallthrough]];
                    case escape_type::usp1_3: {
                        if (!detail::decode_unicode_escape_char(*it, surrogate_pair_1_)) {
                            /* error: invalid char in unicode escape sequence */
                            return on_error(this, error_type::invalid_unicode_escape, it, end);
                        }
                        it++;
                        in_escape_ = (escape_type)((int)in_escape_ + 1);
                        if (in_escape_ <= escape_type::usp1_3) break;
                        assert(in_escape_ == escape_type::usp2_slash);

                        if (0xd800 <= surrogate_pair_1_ && surrogate_pair_1_ <= 0xdbff) {
                            /* surrogate pair start, expect another part */
                            break;
                        }

                        if (0xdc00 <= surrogate_pair_1_ && surrogate_pair_1_ <= 0xdfff) {
                            /* error: invalid Unicode character */
                            return on_error(this, error_type::invalid_unicode_escape, it, end);
                        }

                        in_escape_ = escape_type::none;
                        int32_t codepoint = (int32_t)surrogate_pair_1_;
                        char translated[4];
                        size_t translated_count;
                        if (!detail::char32_to_utf8(codepoint, translated, translated_count)) {
                            /* error: invalid unicode character (?) */
                            return on_error(this, error_type::invalid_unicode_escape, it, end);
                        }
                        value_buffer_.append(translated, translated_count);
                        start = it;
                        break;
                    }

                    case escape_type::usp2_slash: {
                        if (*it != '\\') {
                            /* error: missing second part in unicode surrogate pair */
                            return on_error(this, error_type::invalid_unicode_escape, it, end);
                        }
                        start = it = it + 1;
                        in_escape_ = escape_type::usp2_u;
                        break;
                    }
                    case escape_type::usp2_u: {
                        if (*it != 'u') {
                            /* error: missing second part in unicode surrogate pair */
                            return on_error(this, error_type::invalid_unicode_escape, it, end);
                        }
                        surrogate_pair_2_ = 0;
                        start = it = it + 1;
                        in_escape_ = escape_type::usp2_0;
                        break;
                    }
                    case escape_type::usp2_0: [[fallthrough]];
                    case escape_type::usp2_1: [[fallthrough]];
                    case escape_type::usp2_2: [[fallthrough]];
                    case escape_type::usp2_3: {
                        assert(surrogate_pair_1_ != 0);
                        if (!detail::decode_unicode_escape_char(*it, surrogate_pair_2_)) {
                            /* error: invalid char in unicode escape sequence */
                            return on_error(this, error_type::invalid_unicode_escape, it, end);
                        }
                        it++;
                        in_escape_ = (escape_type)((int)in_escape_ + 1);
                        if (in_escape_ <= escape_type::usp2_3) break;

                        if (0xdc00 > surrogate_pair_2_ || surrogate_pair_2_ > 0xdfff) {
                            /* error: invalid Unicode character */
                            return on_error(this, error_type::invalid_unicode_escape, it, end);
                        }

                        int32_t codepoint = (((int32_t)surrogate_pair_1_ - 0xd800) << 10) +
                                            ( (int32_t)surrogate_pair_2_ - 0xdc00) +
                                            0x10000;
                        char translated[4];
                        size_t translated_count;
                        if (!detail::char32_to_utf8(codepoint, translated, translated_count)) {
                            /* error: invalid unicode character (?) */
                            return on_error(this, error_type::invalid_unicode_escape, it, end);
                        }
                        value_buffer_.append(translated, translated_count);

                        in_escape_ = escape_type::none;
                        start = it;
                        break;
                    }

                    default: assert(false);
                }
            }

            return {true, it};
        }

        /* string value */
        static inline progress_status on_string_value_start(parser_base* p,
                                                            const char* start,
                                                            const char*)
        {
            assert(p->value_buffer_.empty());
            assert(p->value_type_ == value_type::none);
            p->state_push(state::value_string);
            p->value_type_ = value_type::string;
            return {true, start + 1};
        }

        static inline progress_status on_string_value_data(parser_base* p,
                                                           const char* start,
                                                           const char* end)
        {
            maybe_string str;
            auto [ok, it] = p->buffer_string_value(start, end, str);
            if (ok && str.has_value()) {
                p->state_pop();
                switch (p->state()) {
                    case state::array: {
                        ok = on_array_value_parsed(p, str.value());
                        break;
                    }
                    case state::object_key_value: {
                        ok = on_object_value_parsed(p, str.value());
                        break;
                    }
                    default: break;
                }
            }
            return {ok, it};
        }

        /* numbers */
        static inline progress_status on_number_value_start(parser_base* p,
                                                            const char* start,
                                                            const char*)
        {
            assert(p->value_buffer_.empty());
            assert(p->value_type_ == value_type::none);
            p->state_push(state::value_number);
            p->value_type_ = value_type::number;
            return {true, start};
        }

        /* @TODO @LIMIT_RESOURCE_USAGE - don't buffer excesively here */
        static inline progress_status on_number_value_data(parser_base* p,
                                                           const char* start,
                                                           const char* end)
        {
            const char* it = start;
            bool done = false;

            while (it < end && !done) {
                switch (*it) {
                    case '-': [[fallthrough]];
                    case '+': p->looks_like_double_ = true; [[fallthrough]];
                    case '.': p->looks_like_double_ = true; [[fallthrough]];
                    case 'e': p->looks_like_double_ = true; [[fallthrough]];
                    case 'E': p->looks_like_double_ = true; [[fallthrough]];
                    case '0': [[fallthrough]];
                    case '1': [[fallthrough]];
                    case '2': [[fallthrough]];
                    case '3': [[fallthrough]];
                    case '4': [[fallthrough]];
                    case '5': [[fallthrough]];
                    case '6': [[fallthrough]];
                    case '7': [[fallthrough]];
                    case '8': [[fallthrough]];
                    case '9': {
                        it++;
                        break;
                    };
                    default: {
                        done = true;
                        break;
                    }
                }
            }
            assert(start <= it && it <= end);

            if (it == end) return {true, it};
            if (it > start) p->value_buffer_.append(start, it - start);

            union {
                int64_t i;
                double d;
            } value;
            const char* vstart = p->value_buffer_.data();
            const char* vend = vstart + p->value_buffer_.size();
            char* pvend = nullptr;
            bool ok = true;

            errno = 0;
            if (p->looks_like_double_) {
                value.d = std::strtod(vstart, &pvend);
                ok = (errno == 0) && (pvend == vend);
            } else {
                value.i = std::strtoll(vstart, &pvend, 10);
                ok = (errno == 0) && (pvend == vend);
            }

            if (!ok) {
                /* error: invalid numeric value */
                return on_error(p, error_type::invalid_number, start, end);
            }

            p->state_pop();
            switch (p->state()) {
                case state::array: {
                    if (p->looks_like_double_)
                        ok = on_array_value_parsed(p, value.d);
                    else
                        ok = on_array_value_parsed(p, value.i);
                    break;
                }
                case state::object_key_value: {
                    if (p->looks_like_double_)
                        ok = on_object_value_parsed(p, value.d);
                    else
                        ok = on_object_value_parsed(p, value.i);
                    break;
                }
                default: break;
            }
            if (!ok) {
                return on_error(p, error_type::aborted_by_handler, start, end);
            }
            return {true, it};
        }

        /* bool values */
        static inline progress_status on_bool_value_start(parser_base* p,
                                                          const char* start,
                                                          const char*)
        {
            assert(p->value_buffer_.empty());
            assert(p->value_type_ == value_type::none);

            p->state_push(state::value_bool);
            p->value_type_ = value_type::boolean;
            p->value_buffer_.append(1, *start);

            return {true, start + 1};
        }

        static inline progress_status on_bool_value_data(parser_base* p,
                                                         const char* start,
                                                         const char* end)
        {
            assert(!p->value_buffer_.empty());

            const char* it = start;
            size_t max_len = p->value_buffer_.front() == 't'
                             ? detail::array_size(detail::true_chars)
                             : detail::array_size(detail::false_chars);
            bool value;

            while (true) {
                switch (p->value_buffer_.front()) {
                    case 't': {
                        switch (*it) {
                            case 'r': [[fallthrough]];
                            case 'u': [[fallthrough]];
                            case 'e': {
                                p->value_buffer_.append(1, *it);
                                it++;
                                if (p->value_buffer_.size() >= max_len)
                                    goto done_len;
                                if (it == end) return {true, it};
                                break;
                            }
                            default: {
                                /* error: invalid char in bool value */
                                return on_error(p, error_type::invalid_bool, it, end);
                            }
                        }
                        break;
                    }
                    case 'f': {
                        switch (*it) {
                            case 'a': [[fallthrough]];
                            case 'l': [[fallthrough]];
                            case 's': [[fallthrough]];
                            case 'e': {
                                p->value_buffer_.append(1, *it);
                                it++;
                                if (p->value_buffer_.size() >= max_len)
                                    goto done_len;
                                if (it == end) return {true, it};
                                break;
                            }
                            default: {
                                /* error: invalid char in bool value */
                                return on_error(p, error_type::invalid_bool, it, end);
                            }
                        }
                        break;
                    }
                    default: assert(false);
                }
            }

        done_len:
            if (p->value_buffer_.size() < max_len)
                return {true, it};

            if (max_len == sizeof(detail::true_chars)) {
                if (!std::equal(p->value_buffer_.begin(), p->value_buffer_.end(), detail::true_chars)) {
                    /* error: invalid boolean value */
                    return on_error(p, error_type::invalid_bool, start, end);
                }
                value = true;
            } else {
                if (!std::equal(p->value_buffer_.begin(), p->value_buffer_.end(), detail::false_chars)) {
                    /* error: invalid boolean value */
                    return on_error(p, error_type::invalid_bool, start, end);
                }
                value = false;
            }

            p->state_pop();

            bool ok = true;
            switch (p->state()) {
                case state::array: {
                    ok = on_array_value_parsed(p, value);
                    break;
                }
                case state::object_key_value: {
                    ok = on_object_value_parsed(p, value);
                    break;
                }
                default: break;
            }

            return {ok, it};
        }

        /* null values */
        static inline progress_status on_null_value_start(parser_base* p,
                                                          const char* start,
                                                          const char*)
        {
            assert(p->value_buffer_.empty());
            assert(p->value_type_ == value_type::none);

            p->state_push(state::value_null);
            p->value_type_ = value_type::null;
            p->value_buffer_.append(1, *start);

            return {true, start + 1};
        }

        static inline progress_status on_null_value_data(parser_base* p,
                                                         const char* start,
                                                         const char* end)
        {
            assert(!p->value_buffer_.empty());

            const char* it = start;
            size_t max_len = detail::array_size(detail::null_chars);

            while (true) {
                switch (*it) {
                    case 'u': [[fallthrough]];
                    case 'l': {
                        p->value_buffer_.append(1, *it);
                        it++;
                        if (p->value_buffer_.size() >= max_len)
                            goto done_len;
                        if (it == end) return {true, it};
                        break;
                    }
                    default: {
                        /* error: invalid char in 'null' */
                        return on_error(p, error_type::invalid_null, start, end);
                    }
                }
            }

        done_len:
            if (p->value_buffer_.size() < max_len)
                return {true, it};

            if (!std::equal(p->value_buffer_.begin(), p->value_buffer_.end(), detail::null_chars)) {
                /* error: invalid 'null' sequence */
                return on_error(p, error_type::invalid_null, start, end);
            }

            p->state_pop();

            bool ok = true;
            switch (p->state()) {
                case state::array: {
                    ok = on_array_value_parsed(p, nullptr);
                    break;
                }
                case state::object_key_value: {
                    ok = on_object_value_parsed(p, nullptr);
                    break;
                }
                default: break;
            }

            return {ok, it};
        }

        /* object parsing */
        void reset_object_data() {
            object_states_.clear();
        }

        static inline void on_object_value_parsed(parser_base* p,
                                                  [[maybe_unused]] value_type type,
                                                  string& key)
        {
            assert(p->value_type_ == type);
            assert(p->state() == state::object_key_value);
            assert(p->object_states_.size() >= 1);

            object_state& object_state = p->object_states_.back();

            object_state.completed_values += 1;
            p->state_pop();
            key = std::move(object_state.last_key);
            p->reset_value_buffers();
        }

        static inline progress_status on_object_start(parser_base* p,
                                                      const char* start,
                                                      const char* end)
        {
            switch (p->state()) {
                case state::array: {
                    assert(p->array_states_.size() >= 1);
                    array_state& current_array_state = p->array_states_.back();
                    assert(
                        current_array_state.expected_values == current_array_state.completed_values ||
                        current_array_state.expected_values == current_array_state.completed_values + 1
                    );
                    if (!current_array_state.completed_values) {
                        assert(!current_array_state.expected_values);
                        current_array_state.expected_values = 1;
                        current_array_state.completed_values = 1;
                    } else {
                        current_array_state.expected_values = current_array_state.completed_values;
                    }
                    [[fallthrough]];
                }
                case state::toplevel: {
                    if (!p->call_handler_on_object_start(p->depth_, string())) {
                        return on_error(p, error_type::aborted_by_handler, start, end);
                    }
                    break;
                }
                case state::object_key_value: {
                    assert(p->object_states_.size() >= 1);
                    [[maybe_unused]] auto& object_state = p->object_states_.back();
                    string key;
                    on_object_value_parsed(p, value_type::none, key);
                    if (!p->call_handler_on_object_start(p->depth_, key)) {
                        return on_error(p, error_type::aborted_by_handler, start, end);
                    }
                    break;
                }
                default: assert(false);
            }

            p->depth_ += 1;
            p->state_push(state::object);
            p->object_states_.push_back(object_state());
            return {true, start + 1};
        }

        static inline progress_status on_object_key_start(parser_base* p,
                                                          const char* start,
                                                          const char* end)
        {
            assert(p->state() == state::object);
            assert(p->object_states_.size() >= 1);

            object_state& object_state = p->object_states_.back();

            if (object_state.completed_values &&
                    object_state.expected_values != object_state.completed_values + 1) {
                /* error: "VAL KEY" - key follows value without ',' between them */
                return on_error(p, error_type::missing_separator, start, end);
            }

            p->state_push(state::object_key);
            p->value_type_ = value_type::string;

            return {true, start + 1};
        }

        static inline progress_status on_object_key_data(parser_base* p,
                                                         const char* start,
                                                         const char* end)
        {
            assert(p->state() == state::object_key);
            assert(p->object_states_.size() >= 1);

            maybe_string key;
            auto [ok, it] = p->buffer_string_value(start, end, key);
            if (ok && key.has_value()) {
                object_state& object_state = p->object_states_.back();
                object_state.last_key = std::move(key.value());
                p->value_type_ = value_type::none;
                p->state_inplace_transition(state::object_kv_separator);
            }
            return {ok, it};
        }

        static inline progress_status on_object_expects_more_values(parser_base* p,
                                                                    const char* start,
                                                                    const char* end)
        {
            assert(p->state() == state::object);
            assert(p->object_states_.size() >= 1);

            object_state& object_state = p->object_states_.back();
            if (!object_state.completed_values) {
                /* error: "{," - comma right after object start */
                return on_error(p, error_type::early_separator, start, end);
            }

            object_state.expected_values = object_state.completed_values + 1;
            if (object_state.expected_values > object_state.completed_values + 1) {
                /* error: "{.., ,} - comma after comma */
                return on_error(p, error_type::duplicate_separator, start, end);
            }

            return {true, start + 1};
        }

        static inline progress_status on_object_kv_separator(parser_base* p,
                                                             const char* start,
                                                             const char*)
        {
            assert(p->state() == state::object_kv_separator);
            p->state_inplace_transition(state::object_key_value);
            return {true, start + 1};
        }

        static inline bool on_object_value_parsed(parser_base* p, const string& str) {
            string key;
            p->on_object_value_parsed(p, value_type::string, key);
            return p->call_handler_on_object_value_string(key, str);
        }

        static inline bool on_object_value_parsed(parser_base* p, int64_t val) {
            string key;
            p->on_object_value_parsed(p, value_type::number, key);
            return p->call_handler_on_object_value_int(key, val);
        }

        static inline bool on_object_value_parsed(parser_base* p, double val) {
            string key;
            p->on_object_value_parsed(p, value_type::number, key);
            return p->call_handler_on_object_value_double(key, val);
        }

        static inline bool on_object_value_parsed(parser_base* p, bool val) {
            string key;
            p->on_object_value_parsed(p, value_type::boolean, key);
            return p->call_handler_on_object_value_bool(key, val);
        }

        static inline bool on_object_value_parsed(parser_base* p, std::nullptr_t) {
            string key;
            p->on_object_value_parsed(p, value_type::null, key);
            return p->call_handler_on_object_value_null(key);
        }

        static inline progress_status on_object_end(parser_base* p, const char* start, const char* end) {
            assert(p->depth_ >= 1);
            assert(p->state() == state::object);
            assert(p->object_states_.size() >= 1);

            object_state& object_state = p->object_states_.back();
            if (object_state.expected_values > object_state.completed_values) {
                /* error: ",}" - comma right before object end */
                return on_error(p, error_type::key_expected, start, end);
            }

            if (!p->call_handler_on_object_end()) {
                return on_error(p, error_type::aborted_by_handler, start, end);
            }

            p->depth_ -= 1;
            p->object_states_.pop_back();

            p->state_pop();

            return {true, start + 1};
        }

        /* array parsing */
        void reset_array_data() {
            array_states_.clear();
        }

        static inline progress_status on_array_start(parser_base* p,
                                                     const char* start,
                                                     const char* end)
        {
            assert(*start == '[');

            switch (p->state()) {
                case state::array: {
                    assert(p->array_states_.size() >= 1);
                    array_state& current_array_state = p->array_states_.back();
                    assert(
                        current_array_state.expected_values == current_array_state.completed_values ||
                        current_array_state.expected_values == current_array_state.completed_values + 1
                    );
                    if (current_array_state.completed_values &&
                        current_array_state.expected_values != current_array_state.completed_values + 1)
                    {
                        /* error: "VAL VAL" - value follows previous value without ',' between them */
                        return on_error(p, error_type::missing_separator, start, end);
                    }
                    if (!current_array_state.completed_values) {
                        assert(!current_array_state.expected_values);
                        current_array_state.expected_values = 1;
                        current_array_state.completed_values = 1;
                    } else {
                        current_array_state.expected_values = current_array_state.completed_values;
                    }
                    [[fallthrough]];
                }
                case state::toplevel: {
                    if (!p->call_handler_on_array_start(p->depth_, string())) {
                        return on_error(p, error_type::aborted_by_handler, start, end);
                    }
                    break;
                }
                case state::object_key_value: {
                    assert(p->object_states_.size() >= 1);
                    string key;
                    on_object_value_parsed(p, value_type::none, key);
                    if (!p->call_handler_on_array_start(p->depth_, key)) {
                        return on_error(p, error_type::aborted_by_handler, start, end);
                    }
                    break;
                }
                default: assert(false);
            }

            p->depth_ += 1;
            p->array_states_.push_back(array_state());

            p->state_push(state::array);

            return {true, start + 1};
        }

        static inline void on_array_value_parsed(parser_base* p, [[maybe_unused]] value_type type) {
            assert(p->value_type_ == type);
            assert(p->state() == state::array);
            assert(p->array_states_.size() >= 1);

            array_state& current_array_state = p->array_states_.back();
            assert(
                current_array_state.expected_values == current_array_state.completed_values ||
                current_array_state.expected_values == current_array_state.completed_values + 1
            );
            if (!current_array_state.expected_values && !current_array_state.completed_values) {
                current_array_state.completed_values = 1;
                current_array_state.expected_values = 1;
            } else {
                current_array_state.completed_values = current_array_state.expected_values;
            }
            p->reset_value_buffers();
        }

        static inline bool on_array_value_parsed(parser_base* p, const string& value) {
            on_array_value_parsed(p, value_type::string);
            return p->call_handler_on_array_value_string(value);
        }

        static inline bool on_array_value_parsed(parser_base* p, int64_t value) {
            on_array_value_parsed(p, value_type::number);
            return p->call_handler_on_array_value_int(value);
        }

        static inline bool on_array_value_parsed(parser_base* p, double value) {
            on_array_value_parsed(p, value_type::number);
            return p->call_handler_on_array_value_double(value);
        }

        static inline bool on_array_value_parsed(parser_base* p, bool value) {
            on_array_value_parsed(p, value_type::boolean);
            return p->call_handler_on_array_value_bool(value);
        }

        static inline bool on_array_value_parsed(parser_base* p, std::nullptr_t) {
            on_array_value_parsed(p, value_type::null);
            return p->call_handler_on_array_value_null();
        }

        static inline progress_status on_array_expects_more_values(parser_base* p,
                                                                   const char* start,
                                                                   const char* end)
        {
            assert(p->state() == state::array);

            array_state& current_array_state = p->array_states_.back();
            if (!current_array_state.completed_values) {
                /* error: "[," - comma right after array start */
                return on_error(p, error_type::early_separator, start, end);
            }

            current_array_state.expected_values += 1;
            if (current_array_state.expected_values > current_array_state.completed_values + 1) {
                /* error: ", ," - double comma in array */
                return on_error(p, error_type::duplicate_separator, start, end);
            }

            return {true, start + 1};
        }

        static inline progress_status on_array_end(parser_base* p, const char* start, const char* end) {
            assert(p->depth_ >= 1);
            assert(p->array_states_.size() >=1);

            array_state& current_array_state = p->array_states_.back();
            if (current_array_state.expected_values > current_array_state.completed_values) {
                /* error: ",]" - comma right before array end */
                return on_error(p, error_type::value_expected, start, end);
            }

            if (!p->call_handler_on_array_end()) {
                return on_error(p, error_type::aborted_by_handler, start, end);
            }
            p->depth_ -= 1;

            p->array_states_.pop_back();

            assert(p->state() == state::array);
            p->state_pop();

            return {true, start + 1};
        }

        /* fields */
        std::vector<enum state> state_stack_;

        int depth_;
        int line_;
        int column_;
        size_t stream_offset_;

        void internal_reset() {
            depth_ = 0;
            line_ = 1;
            column_ = 1;
            stream_offset_ = 0;

            state_stack_.clear();
            state_stack_.push_back(state::toplevel);

            reset_object_data();
            reset_array_data();
            reset_value_buffers();
        }
    };

    struct stream_event_handler {
        /*
         * depth - 0 for top-level objects, 1+ for nested objects
         * key   - null for top-level objects OR for objects inside arrays
         */
        bool on_object_start(const parser_base&, int, const string&) { return true; }
        bool on_object_end(const parser_base&) { return true; }

        /*
         * depth - 0 for top-level arrays, 1+ for nested arrays
         * key   - null for top-level arrays OR for arrays inside arrays
         */
        bool on_array_start(const parser_base&, int, const string&) { return true; }
        bool on_array_end(const parser_base&) { return true; }

        bool on_object_value_string(const parser_base&, const string&, const string&) { return true; }
        bool on_object_value_int(const parser_base&, const string&, int64_t) { return true; }
        bool on_object_value_double(const parser_base&, const string&, double) { return true; }
        bool on_object_value_bool(const parser_base&, const string&, bool) { return true; }
        bool on_object_value_null(const parser_base&, const string&) { return true; }

        bool on_array_value_string(const parser_base&, const string&) { return true; }
        bool on_array_value_int(const parser_base&, int64_t) { return true; }
        bool on_array_value_double(const parser_base&, double) { return true; }
        bool on_array_value_bool(const parser_base&, bool) { return true; }
        bool on_array_value_null(const parser_base&) { return true; }

        bool on_parse_error(const parser_base&, const parser_error&);

        void reset(const parser_base&);
    };

    template <typename event_handler>
    class parser : public parser_base {
    private:
    public:
        static_assert(std::is_default_constructible_v<event_handler>,
                      "event_handler does not properly implement the stream_event_handler interface");

        using handler_type = event_handler;

        inline parser() {
            internal_reset();
        }
        parser(const parser&) = delete;
        parser(parser&&) = delete;
        parser& operator=(const parser&) = delete;
        parser& operator=(parser&&) = delete;

        void reset() {
            call_handler_reset();
            internal_reset();
        }

    private:
        #define JSSTREAM_METHOD_CHECK(RT, MN, ...) \
        template <typename T> \
        struct has_ ## MN { \
            template <typename U> static char test(decltype(&U::MN)); \
            template <typename U> static int test(...); \
            static const bool has = sizeof(test<T>(0)) == sizeof(char); \
        }

        JSSTREAM_METHOD_CHECK(bool, on_object_start, int, const string&);
        bool call_handler_on_object_start(int depth, const string& key) final {
            if constexpr (has_on_object_start<event_handler>::has) {
                return handler_.on_object_start(*this, depth, key);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_object_end);
        bool call_handler_on_object_end() final {
            if constexpr (has_on_object_end<event_handler>::has) {
                return handler_.on_object_end(*this);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_array_start, int, const string&);
        bool call_handler_on_array_start(int depth, const string& key) final {
            if constexpr (has_on_array_start<event_handler>::has) {
                return handler_.on_array_start(*this, depth, key);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_array_end);
        bool call_handler_on_array_end() final {
            if constexpr (has_on_array_end<event_handler>::has) {
                return handler_.on_array_end(*this);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_object_value_string, const string&, const string&);
        bool call_handler_on_object_value_string(const string& key, const string& value) final {
            if constexpr (has_on_object_value_string<event_handler>::has) {
                return handler_.on_object_value_string(*this, key, value);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_object_value_int, const string&, int64_t);
        bool call_handler_on_object_value_int(const string& key, int64_t value) final {
            if constexpr (has_on_object_value_int<event_handler>::has) {
                return handler_.on_object_value_int(*this, key, value);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_object_value_double, const string&, double);
        bool call_handler_on_object_value_double(const string& key, double value) final {
            if constexpr (has_on_object_value_double<event_handler>::has){
                return handler_.on_object_value_double(*this, key, value);
            } else {
                return  true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_object_value_bool, const string&, bool);
        bool call_handler_on_object_value_bool(const string& key, bool value) final {
            if constexpr (has_on_object_value_bool<event_handler>::has) {
                return handler_.on_object_value_bool(*this, key, value);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_object_value_null, const string&);
        bool call_handler_on_object_value_null(const string& key) final {
            if constexpr (has_on_object_value_null<event_handler>::has) {
                return handler_.on_object_value_null(*this, key);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_array_value_string, const string&);
        bool call_handler_on_array_value_string(const string& value) final {
            if constexpr (has_on_array_value_string<event_handler>::has) {
                return handler_.on_array_value_string(*this, value);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_array_value_int, int64_t);
        bool call_handler_on_array_value_int(int64_t value) final {
            if constexpr (has_on_array_value_int<event_handler>::has) {
                return handler_.on_array_value_int(*this, value);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_array_value_double, double);
        bool call_handler_on_array_value_double(double value) final {
            if constexpr (has_on_array_value_double<event_handler>::has) {
                return handler_.on_array_value_double(*this, value);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_array_value_bool, bool);
        bool call_handler_on_array_value_bool(bool value) final {
            if constexpr (has_on_array_value_bool<event_handler>::has) {
                return handler_.on_array_value_bool(*this, value);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(bool, on_array_value_null);
        bool call_handler_on_array_value_null() final {
            if constexpr (has_on_array_value_null<event_handler>::has) {
                return handler_.on_array_value_null(*this);
            } else {
                return true;
            }
        }

        JSSTREAM_METHOD_CHECK(bool, on_parse_error, const parser_error&);
        bool call_handler_on_parse_error(const parser_error& error) final {
            if constexpr (has_on_parse_error<event_handler>::has) {
                return handler_.on_parse_error(*this, error);
            } else {
                return true;
            }
        }
        JSSTREAM_METHOD_CHECK(void, reset);
        void call_handler_reset() final {
            if constexpr (has_reset<event_handler>::has) {
                handler_.reset(*this);
            }
        }
        #undef JSSTREAM_METHOD_CHECK

        event_handler handler_{};
    };
}
