#include <jsstream/jsstream.h>
#include <cstdio>
#include <cstring>
#include <string>

struct handler_t {
    int last_depth{-1};

    static const jsstream::string& actual_key(const jsstream::parser_base& parser,
                                              [[maybe_unused]] int depth,
                                              const jsstream::string& key) {
        static constexpr const char top[] = "[TOPLEVEL]";
        static constexpr const size_t toplen = sizeof(top) - 1;
        static constexpr const char arr[] = "[IN_ARRAY]";
        static constexpr const size_t arrlen = sizeof(arr) - 1;

        static jsstream::string s_top(top, toplen);
        static jsstream::string s_arr(arr, arrlen);

        auto state = parser.state();
        switch (state) {
            case jsstream::state::toplevel: {
                assert(depth == 0);
                return s_top;
            }
            case jsstream::state::array: {
                assert(depth >= 1);
                return s_arr;
            }
            default: {
                assert(depth >= 1);
                assert(state == jsstream::state::object);
                return key;
            }
        }
    }

    bool on_object_start(const jsstream::parser_base& parser,
                         int depth,
                         const jsstream::string& key) {
        assert(depth == last_depth + 1);

        auto& a_key = actual_key(parser, depth, key);
        last_depth += 1;

        std::printf("%*s>>> OBJECT (%.*s) (%d :: %d)\n",
                    last_depth * 2, "",
                    (int)a_key.size(), a_key.data(),
                    depth,
                    last_depth);
        return true;
    }

    bool on_object_end(const jsstream::parser_base&) {
        assert(last_depth >= 0);
        std::printf("%*s<<< OBJECT\n", last_depth * 2, "");

        last_depth -= 1;
        return true;
    }

    bool on_array_start(const jsstream::parser_base& parser,
                        int depth,
                        const jsstream::string& key) {
        assert(depth == last_depth + 1);

        auto& a_key = actual_key(parser, depth, key);
        last_depth += 1;

        std::printf("%*s>>> ARRAY (%.*s) (%d :: %d)\n",
                    last_depth * 2, "",
                    (int)a_key.size(), a_key.data(),
                    depth,
                    last_depth);
        return true;
    }

    bool on_array_end(const jsstream::parser_base&) {
        assert(last_depth >= 0);
        std::printf("%*s<<< ARRAY\n", last_depth * 2, "");

        last_depth -= 1;
        return true;
    }

    bool on_array_value_string(const jsstream::parser_base&, const jsstream::string& value) {
        std::printf("%*s*** STRING (ARRAY MEMBER): \"%.*s\"\n",
                    (last_depth + 1) * 2, "",
                    (int)value.size(), value.data());
        return true;
    }
    bool on_array_value_int(const jsstream::parser_base&, int64_t value) {
        std::printf("%*s*** INTEGER (ARRAY MEMBER): %lld\n",
                    (last_depth + 1) * 2, "",
                    (long long)value);
        return true;
    }
    bool on_array_value_double(const jsstream::parser_base&, double value) {
        std::printf("%*s*** DOUBLE (ARRAY MEMBER): %lf\n",
                    (last_depth + 1) * 2, "",
                    value);
        return true;
    }
    bool on_array_value_bool(const jsstream::parser_base&, bool value) {
        std::printf("%*s*** BOOLEAN (ARRAY MEMBER): %s\n",
                    (last_depth + 1) * 2, "",
                    value ? "true" : "false");
        return true;
    }
    bool on_array_value_null(const jsstream::parser_base&) {
        std::printf("%*s*** NULL (ARRAY MEMBER)\n",
                    (last_depth + 1) * 2, "");
        return true;
    }

    bool on_object_value_string(const jsstream::parser_base&,
                                const jsstream::string& key, const jsstream::string& value) {
        std::printf("%*s*** STRING (OBJECT MEMBER): \"%.*s\" = \"%.*s\" \n",
                    (last_depth + 1) * 2, "",
                    (int)key.size(), key.data(),
                    (int)value.size(), value.data());
        return true;
    }
    bool on_object_value_int(const jsstream::parser_base&, const jsstream::string& key, int64_t value) {
        std::printf("%*s*** INTEGER (OBJECT MEMBER): \"%.*s\" = %lld\n",
                    (last_depth + 1) * 2, "",
                    (int)key.size(), key.data(),
                    (long long)value);
        return true;
    }
    bool on_object_value_double(const jsstream::parser_base&, const jsstream::string& key, double value) {
        std::printf("%*s*** DOUBLE (OBJECT MEMBER): \"%.*s\" = %lf\n",
                    (last_depth + 1) * 2, "",
                    (int)key.size(), key.data(),
                    value);
        return true;
    }
    bool on_object_value_bool(const jsstream::parser_base&, const jsstream::string& key, bool value) {
        std::printf("%*s*** BOOLEAN (OBJECT MEMBER): \"%.*s\" = %s\n",
                    (last_depth + 1) * 2, "",
                    (int)key.size(), key.data(),
                    value ? "true" : "false");
        return true;
    }
    bool on_object_value_null(const jsstream::parser_base&, const jsstream::string& key) {
        std::printf("%*s*** NULL (OBJECT MEMBER): \"%.*s\" = null\n",
                    (last_depth + 1) * 2, "",
                    (int)key.size(), key.data());
        return true;
    }

    void reset(const jsstream::parser_base&) {
        std::printf(">>>>> RESET <<<<<\n");
        last_depth = -1;
    }

    bool on_parse_error(const jsstream::parser_base&, const jsstream::parser_error& error) {
        std::printf(">>PARSER ERROR<<: %d:%d (%zd) : %s\n",
                    error.line, error.column, error.stream_offset,
                    error.to_string());
        return false;
    }
};

static const char* buffers[] = {
    R"( ["\uD83E\uDD14"] )",
    "",

    R"( ["\)", R"(n\t"] )",
    "",

    R"( [ "\uD)", R"(8)", R"(3D\u)", R"(DE01fUser \uD83)", R"(C)", R"(\u)", R"(DDEC\uD83)", R"(C\uDD)", R"(E7{\)", R"(n\tfa575ef7-e69c-489a-ac66-f89753051442 gigi parola\n} requested note list" ] )",
    "",

    u8R"( {"a": "b"} )",
    u8R"( [1,2,3] )",
    "",

    R"({ "duration": {"connect": 0)", R"(, "request": 28 } } )",
    R"({ "duration": {"connect": tru)", R"(e, "request": 28 } } )",
    "",

    u8R"(
    {"level":"info","msg":"API engine engaged","time":"2018-09-20T22:14:39+03:00"}
    {"correlationId":"7a99d9f1-1525-468d-9ee8-b574c06a8a69","handler":"Authorization","level":"warning","msg":"Failed to extract claims from JWT token. Reason :square/go-jose: error in cryptographic primitive","time":"2018-09-20T22:14:49+03:00"}
    {"correlationId":"e00b71d0-36a7-4286-b50f-d882896ba559","handler":"Authorization","level":"info","msg":"Request author successfully authenticated as map[id:fa575ef7-e69c-489a-ac66-f89753051442 username:gigi]","time":"2018-09-20T22:15:15+03:00"}
    {"correlationId":"e00b71d0-36a7-4286-b50f-d882896ba559","handler":"Notes","level":"info","msg":"User \u0026{fa575ef7-e69c-489a-ac66-f89753051442 gigi parola} requested note list","time":"2018-09-20T22:15:15+03:00"}
    )",
    "",

    "[n", "ull, nu", "ll, nul", "l", ",null]",
    "",

    "{\"null\": null}",
    "{\"true\": tru", "e, \"false\": fa", "lse}",
    "[f", "alse, tr", "ue]",
    "{\"some_val\": 123, \"some_other_val\" : 787.e-3}\n",
    "[[ 123, 0.123 ] , {\"a\" : \"b\", \"array_key\"   :   \t\t [{}], \"k\": null}, \"this is a test\"]\n",
    "{\"a\" : \"b\" , \"a\" : [\"b\", true, false,null]}\n",
    "[\"a\" , \"b\"]\n",
};

using stream_t = jsstream::parser<handler_t>;

int main()
{
    stream_t p;
    size_t total_size = 0;

    std::printf("obj = %zd (%zd)\n", sizeof(p), sizeof(stream_t::handler_type));
    std::printf("%zd %zd\n", sizeof(std::string), sizeof(jsstream::string));

    for (size_t i = 0; i < sizeof(buffers) / sizeof(buffers[0]); i++) {
        size_t len = std::strlen(buffers[i]);
        total_size += len;

        if (!len) {
            std::printf("line: %d, col: %d (stream offset: %zd / %zd)\n\n",
                        p.line(), p.column(),
                        p.stream_offset(), total_size);
            p.reset();
            total_size = len;
        }
        if (!p.feed(buffers[i], len))
            std::printf("\nFAILED at buffer %zd\n", i);
    }
    std::printf("line: %d, col: %d (stream offset: %zd / %zd)\n",
                p.line(), p.column(),
                p.stream_offset(), total_size);

    return 0;
}
